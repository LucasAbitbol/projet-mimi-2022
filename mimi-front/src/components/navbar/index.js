import React from 'react'
import { NavLink as Link} from 'react-router-dom'
import {Nav,Bars, NavMenu,NavLink,NavBtn,NavBtnLink} from './NavbarElements';
import Logo from "../Logo";


const Navbar = () => {
  return (
    <Nav>
        <NavLink to="/MenuEleve">
        <img src="./img/logoCHL.png" alt="logo" style={{width:'100px', height:'100px;', transform: 'translate(30%)'}}/>
        </NavLink>
          <Bars />
          <NavMenu>
            <NavLink to="/cours" activeStyle >
                Cours
            </NavLink>
            <NavLink to="/visio" activeStyle>
                Visioconférence
            </NavLink>
            <NavLink to="/avatar" activeStyle>
                Avatar
            </NavLink>
            <NavLink to="/jeux" activeStyle>
                Jeux
            </NavLink>
          </NavMenu>
          <NavBtn>
              <NavBtnLink to="/" style={{fontWeight: 'bold', transform: 'translate(0%)', fontFamily: 'Lucida Handwriting'}}>Se déconnecter</NavBtnLink>
          </NavBtn>
        
    </Nav>
  )
}

export default Navbar