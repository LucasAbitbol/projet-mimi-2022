import React from 'react';
import Connexion from '../pages/Connexion';

const Logo = () => {
    return(
        <div className = "logo">
           <a href="/"><img src="./img/logoCHL.png" alt="logo" /></a>
        </div>
    );
};

export default Logo;