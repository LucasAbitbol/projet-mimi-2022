import React from 'react';
import Navbar from '../components/navbar/index';

const Avatar = () => {
    return(
        <div className = "avatar">
            <Navbar />
           <div className="columns is-full is-mobile is-vcentered total">
                <div className="column is-half ligneAvatar">

                    <img src="./img/mii.png" alt="mii" style={{width:'110px', marginRight:'50px'}}/>
                    <img src="./img/photo.png" alt="camera" style={{width:'125px', marginRight:'25px'}}/>
                    <img src="./img/a.png" alt="a" style={{width:'150px', marginLeft:'25px'}}/>
                    <img src="./img/facecam.png" alt="face" style={{width:'150px', marginLeft:'50px'}}/>

                <div className="texte">
                    <div className="texteMii" style={{transform: 'translate(-885%)'}}>
                        <p>Mii</p>
                    </div>
                    <div className="textePhoto" style={{transform: 'translate(-115%)'}}>
                        <p>PHOTO</p>
                    </div>
                    <div className="texteA" style={{transform: 'translate(+110%)'}}>
                        <p>AVATAR</p>
                    </div>
                    <div className="texteFace" style={{transform: 'translate(+260%)'}}>
                        <p>FACECAM</p>
                    </div>
                </div>

                </div>
           </div>
        </div>
    );
};

export default Avatar;