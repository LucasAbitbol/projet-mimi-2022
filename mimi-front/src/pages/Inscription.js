import React from 'react';
import Logo from "../components/Logo";
import '../styles/components/_inscription.scss';


const Inscription = () => {
    return(
<div> 
<div className = "connexion">
            <div className = "box">
                <div className="columns is-full is-mobile" > 
                    <div className="column is-half coteDroit">
                    <h1>INSCRIPTION</h1>
                    <br></br>
                        <h2>NOM ETABLISSEMENT</h2>
                        <div className="nometablissement">
                            <input
                            className="input is-rounded"
                            type="nom"
                            id="nometablissement"
                            placeholder="Nom Etablissement"
                            required
                            />              
                        </div>

                        <h2>REFERENCE CLIENT ETABLISSEMENT</h2>
                        <div className="refetablissement">
                            <input
                            className="input is-rounded"
                            type="nom"
                            id="refetablissement"
                            placeholder="Ref. Etablissement"
                            required
                            />              
                        </div>

                        <h2>CODE POSTAL</h2>
                        <div className="etablissement">
                            <input
                            className="input is-rounded"
                            type="number"
                            id="cp"
                            placeholder="Code Postal"
                            min="10000"
                            max="99999"
                            required
                            />              
                        </div>
                        
                        <h2>CONTACT ETABLISSEMENT</h2>
                        <div className="nom">
                            <input
                            className="input is-rounded"
                            type="nom"
                            id="contactetablissement"
                            placeholder="Tél. ou e-mail Etablissement"
                            required
                            />              
                        </div>
                        

                        
                            
                        <br></br>
                        <a className={`button boutonConnexion`} href="/inscription2">
                        SUITE
                        </a>
                        <br></br>
                    </div>
                    <Logo />  
                </div>
            </div>
        </div>



</div>

        );
    };
    
    export default Inscription;

