import React from 'react';

import Logo from "../components/Logo";
import '../styles/components/_endform.scss';


const Endform = () => {
    return(
<div>
<div className = "connexion">
            <div className = "box">
                <div className="columns is-full is-mobile" > 
                    <div className="column is-half coteDroit">
                    <h1>INCRIPTION TERMINEE</h1>
                    <br></br>
                    <h3>Votre inscription est désormais complète.</h3>

                    <h3>Nous allons vous contacter ainsi que l'établissement scolaire,<br />afin  de pouvoir mettre en place la solution MiMi.</h3>

                    <h3>Vérifiez régulièrement vos e-mails.<br /> Nous risquons de vous contacter d'ici X jours.</h3>
                       
                    </div>
                   <div className='log'> <Logo />  </div>
                </div>
            </div>
        </div>
</div>
        );
    };
    
    export default Endform;