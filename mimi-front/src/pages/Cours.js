import React from 'react';
import Navbar from '../components/navbar/index';

const Cours = () => {
    return(
        
        <div className = "cours">
            <div> <Navbar /></div>
           <div className="columns is-full is-mobile is-vcentered total">
                <div className="column is-half ligne1">
                    
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginRight:'50px'}}/>
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginRight:'25px'}}/>
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginLeft:'25px'}}/>
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginLeft:'50px'}}/>
                    
                </div>

                <div className="texte">
                    <div className="texteMath" style={{transform: 'translate(-160%)'}}>
                        <p>MATHEMATIQUES</p>
                    </div>
                    <div className="texteFrancais" style={{transform: 'translate(-145%)'}}>
                        <p>FRANCAIS</p>
                    </div>
                    <div className="texteSvt" style={{transform: 'translate(+125%)'}}>
                        <p>SVT</p>
                    </div>
                    <div className="textePhysique" style={{transform: 'translate(+250%)'}}>
                        <p>PHYSIQUE</p>
                    </div>
                </div>

                <div className="column is-half ligne2">
                    
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginRight:'50px'}}/>
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginRight:'25px'}}/>
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginLeft:'25px'}}/>
                    <img src="./img/dossier.png" alt="cours" style={{width:'150px', height:'150px;', marginLeft:'50px'}}/>

                </div>

                <div className="texte">
                <div className="texteAnglais" style={{transform: 'translate(-310%)'}}>
                        <p>ANGLAIS</p>
                    </div>
                    <div className="texteEspagnol" style={{transform: 'translate(-90%)'}}>
                        <p>ESPAGNOL</p>
                    </div>
                    <div className="texteHistoire" style={{transform: 'translate(+80%)'}}>
                        <p>HISTOIRE</p>
                    </div>
                    <div className="texteChimie" style={{transform: 'translate(+360%)'}}>
                        <p>CHIMIE</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Cours;