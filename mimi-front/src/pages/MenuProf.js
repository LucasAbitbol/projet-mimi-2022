import React from 'react';
import '../styles/components/_menuProf.scss';

const MenuProf = () => {
    return(
        <div className = "menu">
            <a className={`button boutonDeconnexion`} href="/" style={{fontWeight: 'bold', fontFamily: 'Lucida Handwriting'}}>
            Se déconnecter
            </a>
           <div className="columns is-full is-mobile is-vcentered total">
                <div className="column is-half ligne">
                <a href="/visioProf">
                    <img src="./img/video.png" alt="video" style={{width:'151px', height:'151px;', marginRight:'150px'}}/>
                </a>
                <a href="/depot">
                    <img src="./img/envoi.png" alt="envoi" style={{width:'151px', height:'151px;', marginLeft:'150px'}}/>
                </a>

                <div className="texte">
                    <div className="texteVisioProf">
                        <p>DEPOSER DES COURS</p>
                    </div>
                    <div className="texteEnvoi">
                        <p>VISIOCONFERENCE</p>
                    </div>
                </div>

                </div>
           </div>
        </div>
    );
};

export default MenuProf;