import React from 'react';
import Logo from "../components/Logo";
import '../styles/components/_inscription.scss';


const Inscription2 = () => {
    return(
<div> 
<div className = "connexion">
            <div className = "box">
                <div className="columns is-full is-mobile" > 
                    <div className="column is-half coteDroit">
                    <h1>INSCRIPTION</h1>
                    <br></br>
                    <h2>NOM ENFANT</h2>
                        <div className="name">
                            <input
                            className="input is-rounded"
                            type="name"
                            id="name"
                            placeholder="Nom Enfant"
                            required
                            />            
                        </div>
                        <h2>PRENOM ENFANT</h2>
                        <div className="prenom">
                            <input
                            className="input is-rounded"
                            type="prenom"
                            id="prenom"
                            placeholder="Prénom Enfant"
                            required
                            />
                            </div>  
                            <h2>DATE NAISSANCE</h2>
                        <div className="datenaissance">
                            <input
                            className="input is-rounded"
                            type="date"
                            id="datenaissance"
                            placeholder=""
                            required
                            />
                            </div> 
                        
                        <h2>IDENTIFIANT</h2>
                        <div className="id">
                            <input
                            className="input is-rounded"
                            type="email"
                            id="email"
                            placeholder="Email"
                            required
                            />            
                        </div>

                        <h2>MOT DE PASSE</h2>
                        <div className="mdp">
                            <input
                            className="input is-rounded"
                            type="password"
                            id="password"
                            placeholder="Mot de passe"
                            required
                            />            
                        </div>
                        <h2>CONFIRMATION MOT DE PASSE</h2>
                        <div className="mdp">
                            <input
                            className="input is-rounded"
                            type="password2"
                            id="password2"
                            placeholder="Confirmer mot de passe"
                            required
                            />            
                        </div>
                            
                        <br></br>
                        <a className={`button boutonConnexion`} href="/inscription3">
                        SUITE
                        </a>
                        <br></br>
                    </div>
                    <Logo />  
                </div>
            </div>
        </div>



</div>

        );
    };
    
    export default Inscription2;

