import React from 'react';
import Navbar from '../components/navbar/index';
import '../styles/components/_visio.scss';

const Visio = () => {
    return(
        <>
        <Navbar />
        <div className="visio">
        <img src="./img/visio.png" alt="visio" style={{width:'950px', height:'950px;',marginTop:'50px',transform: 'translate(16%)'}}/>   
        </div>
        </>
    );
};

export default Visio;