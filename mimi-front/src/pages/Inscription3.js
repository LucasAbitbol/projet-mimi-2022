import React from 'react';
import Logo from "../components/Logo";
import '../styles/components/_inscription.scss';


const Inscription3 = () => {
    return(
<div> 
<div className = "connexion">
            <div className = "box">
                <div className="columns is-full is-mobile" > 
                    <div className="column is-half coteDroit">
                    <h1>INSCRIPTION</h1>
                    <br></br>
                            <h2>NOM PARENT</h2>
                        <div className="nom">
                            <input
                            className="input is-rounded"
                            type="nom"
                            id="nomparent"
                            placeholder="Nom Parent"
                            required
                            />              
                        </div>          
                        <h2>PRENOM PARENT</h2>
                        <div className="prenom">
                            <input
                            className="input is-rounded"
                            type="prenom"
                            id="prenomparent"
                            placeholder="Prénom Parent"
                            required
                            />
                            </div>
                            <h2>NUMERO DE TELEPHONE</h2>
                        <div className="tel">
                            <input
                            className="input is-rounded"
                            type="numero"
                            id="tel"
                            placeholder="Tél."
                            required
                            />              
                        </div>
                        
                       
                        
                            
                        <br></br>
                        <a className={`button boutonConnexion`} href="/endform">
                        S'INSCRIRE
                        </a>
                        <br></br>
                    </div>
                    <Logo />  
                </div>
            </div>
        </div>



</div>

        );
    };
    
    export default Inscription3;

