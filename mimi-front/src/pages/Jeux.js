import React from 'react';
import Navbar from '../components/navbar/index';


const Jeux = () => {

    /* const buttons = document.querySelectorAll("button");

    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", function() {
            const joueur = buttons[i].innerHTML;
            const robot = buttons[Math.floor(Math.random() * buttons.length)].innerHTML; 
            let resultat = ""; 
            console.log(`Joueur : ${joueur} VS Robot : ${robot}`);

           if(joueur === robot) {
                resultat = "Egalité";
            }
            else if ((joueur === "Pierre" && robot === "Ciseaux") || (joueur === "Feuille" && robot === "Pierre") || (joueur === "Ciseaux" && robot === "Feuille")){
                resultat = "Gagné !"
            }
            else {
                resultat = "Perdu..."
            }

            document.querySelectorAll(".resultat").innerHTML = `Joueur : ${joueur} </br> Robot : ${robot} </br> ${resultat} `;
        });
    } */
    
    return(
        <>  <div>
            <Navbar />
            </div>
            <div className="jeux" style={{backgroundImage:'url(./img/etoile.png)'}}>
            <br></br>
            {/* <button >Pierre</button>
            <button>Feuille</button>
            <button>Ciseaux</button>
            <div className="resultat"></div> */}

            <div className="columns is-full is-mobile is-vcentered total">
            <div className="column is-half ligneJeux" style={{textAlign:"center", marginTop:"15%"}}>
            <a href="https://www.tusmo.xyz/">
            <img src="./img/tusmo.png" alt="tusmo" style={{width:'200px', borderRadius:'5%', marginRight:'50px'}}/* ,left:'30%',top:'-100%',position: 'relative' *//>
            </a>
            <a href="https://www.loups-garous-en-ligne.com/"> 
            <img src="./img/loup-garou.jpg" alt="loup"style={{width:'200px', borderRadius:'10%', marginRight:'25px'}}/* , position: 'relative',left:'30%',top:'-25%'}} *//>
            </a>
            <a href="https://petitbac.net/">
            <img src="./img/petitbac.png" alt="petitbac" style={{width:'200px', borderRadius:'5%', marginLeft:'25px'}}/* ,position: 'relative',left:'40%',top:'-25%'}} *//>
            </a>
            <a href="https://garticphone.com/fr">
            <img src="./img/garticphone.jpg" alt="garticphone" style={{width:'200px', borderRadius:'5%', marginLeft:'50px'}}/* ,position: 'relative',left:'40%',top:'-25%'}} *//>
            </a>
        </div>
        </div>
        <img src="./img/etoile.png" alt="étoile" style={{width:'500px', position: 'relative',left:'0%', top:'0%'}}/>
        </div>
        </>
    )
    };
export default Jeux;