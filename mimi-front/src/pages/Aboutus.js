import React from 'react';
import Logo from "../components/Logo";
import '../styles/components/_endform.scss';


const Aboutus = () => {
    return(
<div>
<div className = "connexion">
            <div className = "box">
                <div className="columns is-full is-mobile" > 
                    <div className="column is-half coteDroit">
                    <h1>QUI SOMMES NOUS ?</h1>
                    <br></br>
                    <h3>L'AdPep sert à aider les étudiants de primaire jusqu'au lycée<br /> qui ne peuvent se rendre en cours physiquement.</h3>

                    <h3>Le but de cette application est d'offrir aux étudiants un dispositif personnalisé<br />Afin qu'ils se sentent mieux dans le cadre scolaire.</h3>

                    <h3>N'hésitez pas à nous contacter au 06 13 13 13 13 pour plus de renseignements.<br /> Notre site Web <a href='https://www.lespep81.fr/'>ICI</a> </h3>
                       
                    </div>
                    <Logo />  
                </div>
            </div>
        </div>
</div>
        );
    };
    
    export default Aboutus;