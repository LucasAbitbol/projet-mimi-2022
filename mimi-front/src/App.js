import './App.css';
import React from "react";
import {Switch, BrowserRouter, Route} from 'react-router-dom'
import Connexion from "./pages/Connexion";
import MenuEleve from "./pages/MenuEleve";
import Cours from "./pages/Cours";
import Avatar from "./pages/Avatar";
import Jeux from "./pages/Jeux";
import Visio from "./pages/Visio";
import MenuProf from "./pages/MenuProf";
import Navbar from './components/navbar/index';
import DepotCours from './pages/DepotCours';
import Inscription from './pages/Inscription';
import Endform from './pages/Endform';
import Aboutus from './pages/Aboutus';
import Inscription2 from './pages/Inscription2';
import Inscription3 from './pages/Inscription3';
import VisioProf from './pages/VisioProf';
import Service from "./Service";

const App = () => {
  return (
    <>
    <BrowserRouter>
      <Switch> 
        <Route path="/" exact component={Connexion} />
        <Route path="/menuEleve" exact component={MenuEleve} />
        <Route path="/cours" exact component={Cours} />
        <Route path="/jeux" exact component={Jeux} />
        <Route path="/avatar" exact component={Avatar} />
        <Route path="/visio" exact component={Visio} />
        <Route path="/menuProf" exact component={MenuProf} />
        <Route path="/depot" exact component={DepotCours} />
        <Route path="/inscription" exact component={Inscription} />
        <Route path="/endform" exact component={Endform} />
        <Route path='/aboutus' exact component={Aboutus} />
        <Route path='/inscription2' exact component={Inscription2} />
        <Route path='/inscription3' exact component={Inscription3} />
        <Route path='/visioProf' exact component={VisioProf} />
      </Switch>
    </BrowserRouter>
{/*     <div className="App">
      <header className="App-header">
        <Service />
      </header>  
    </div> */}
    </>
  );
}

export default App;
