const Student = require('../models/student');

exports.createStudent = (req, res, next) => {
  const student = new Student({
    lastname: req.body.lastname,
    firstname: req.body.firstname,
    icone: req.body.icone,
    course: req.body.course,
    professorId: req.body.professorId,
    email: req.body.email,
    password: req.body.password
  });
  student.save().then(
    () => {
      res.status(201).json({
        message: 'Etudiant enregistré avec succès!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getOneStudent = (req, res, next) => {
  Student.findOne({
    _id: req.params.id
  }).then(
    (student) => {
      res.status(200).json(student);
      console.log(student);
    }
  ).catch(
    (error) => {
      res.status(404).json({
        error: error
      });
    }
  );
};

exports.modifyStudent = (req, res, next) => {
  const student = new Student({
    _id: req.params.id,
    lastname: req.body.lastname,
    firstname: req.body.firstname,
    icone: req.body.icone,
    course: req.body.course,
    professorId: req.body.professorId,
    email: req.body.email,
    password: req.body.password
  });
  Student.updateOne({_id: req.params.id}, student).then(
    () => {
      res.status(201).json({
        message: 'Etudiant modifié avec succès!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.deleteStudent = (req, res, next) => {
  Student.deleteOne({_id: req.params.id}).then(
    () => {
      res.status(200).json({
        message: 'Supprimé!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getAllStudent = (req, res, next) => {
  Student.find().then(
    (students) => {
      res.status(200).json(students);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};