const express = require('express');
const mongoose = require('mongoose');

const app = express();

const Student = require('./models/student');
const userRoutes = require('./routes/user');

mongoose.connect('mongodb+srv://GroupeMimi:projetmimi2022@db-mimi.j1ywi.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use(express.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

const studentRoutes = require('./routes/student');

app.use('/api/auth', userRoutes);
app.use('/api/student', studentRoutes);
  
module.exports = app;