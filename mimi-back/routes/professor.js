const express = require('express');
const router = express.Router();

const professorCtrl = require('../controllers/professor');

router.post('/signup', professorCtrl.signup);
router.post('/login', professorCtrl.login);

module.exports = router;