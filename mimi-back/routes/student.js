const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');

const studentCtrl = require('../controllers/student');

router.get('/', auth, studentCtrl.getAllStudent);
router.post('/', auth, studentCtrl.createStudent);
router.get('/:id', auth, studentCtrl.getOneStudent);
router.put('/:id', auth, studentCtrl.modifyStudent);
router.delete('/:id', auth, studentCtrl.deleteStudent);

module.exports = router;