const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const studentSchema = mongoose.Schema({
  _id: { type: String, required: true },
  lastname: { type: String, required: false },
  firstname: { type: String, required: false },
  icone: {type: String, required: false},
  course: { type: String, required: false },
  professorId: { type: String, required: false },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
});

studentSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Student', studentSchema);
